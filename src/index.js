import React, { useState } from "react";
import ReactDOM from "react-dom";
import ReactTooltip from "react-tooltip";

import "./styles.css";

import MapChart from "./MapChart";
import MapChart2 from "./MapChart_2";

function App() {
  const [content, setContent] = useState("");
  const [content2, setContent2] = useState("");
  return (
    <div>
      <div>
        <MapChart setTooltipContent={setContent2} />
        <ReactTooltip>{content2}</ReactTooltip>
      </div>
      <div>
        <MapChart2 setTooltipContent={setContent} />
        <ReactTooltip>{content}</ReactTooltip>
      </div>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
